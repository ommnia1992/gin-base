package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
)

const (
	host     = "db"
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbname   = "TestDB"
)

var Connect *gorm.DB


func Database() (*gorm.DB, error) {

	dbInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

		db, err := gorm.Open("postgres", dbInfo)

		if err != nil {
			return nil, err
			//log.Fatalf("Database error: %s\n", err)
		}

		return db, nil
}


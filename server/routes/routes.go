package routes

import "github.com/gin-gonic/gin"

var Router = gin.Default()

func InitRoutes(){

	routesBasic:=Router.Group("/")

	routesBasic.GET("/", HomeHandler)

	Router.NoRoute(NotFoundHandler)
	Router.LoadHTMLGlob("./template/*.html")

}

func HomeHandler(c *gin.Context){
	c.HTML(200,"index.html", gin.H{
		"title": "Home page",
	})
}

func NotFoundHandler(c *gin.Context){
	c.HTML(404, "404.html", gin.H{
		"title":"404 page",
	})
}
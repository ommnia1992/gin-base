package main

import (
	"./routes"
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"net/http"
	"os"
	"time"
	"./db"
)

func main() {
	var err error
	//var err error
	db.Connect, err = db.Database()
	if err!=nil  {
		log.Fatal(err)
	}

	port := os.Getenv("PORT")
	fmt.Println(port)
	if port == "" {
		port = "8080"
	}

	serverConf := &http.Server{
		Addr:           ":" + port,
		Handler:        routes.Router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	routes.InitRoutes()

	log.Fatal(serverConf.ListenAndServe())
}

#Stable
FROM golang:1.12.9-alpine3.10 AS build
RUN apk update && apk --no-cache add gcc g++ make && apk add --no-cache git
RUN mkdir -p /go/src/app
WORKDIR /go/src/app
COPY . .
ENV PORT=5000
RUN  go get github.com/jinzhu/gorm  \
 github.com/gin-gonic/gin \
 github.com/jinzhu/gorm/dialects/postgres
RUN GOOS=linux go build  ./server/main.go
RUN ls


FROM alpine:3.10
RUN apk --no-cache add ca-certificates
COPY --from=build /go/src/app/main .
WORKDIR /go/src/app
RUN ls
EXPOSE 8080
ENTRYPOINT ["/main"]




